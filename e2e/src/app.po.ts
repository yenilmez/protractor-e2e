import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  navigateToAngularIo() {
    return browser.get('https://angular.io/tutorial') as Promise<any>;
  }

  getTitleText() {
    return element(by.css('app-root .content span')).getText() as Promise<string>;
  }

  getTitleAngular() {
    return element(by.css('mat-sidenav-content h1')).getText() as Promise<string>;
  }

  getGithubRedirectButton() {
    return element(by.css('.github-links a'));
  }

  getUsernameInput() {
    return element(by.name('login'));
  }

  getPasswordInput() {
    return element(by.name('password'));
  }

  getSubmitButton() {
    return element(by.name('commit'));
  }

  getGithubShelfTitle() {
    return element(by.css('.shelf-title')).getText();
  }

  getParagraph() {
    return element.all(by.css('div.content p'));
  }

  getH2Title() {
    return element.all(by.css('div.content[role="main"] h2'));
  }
}
