import { AppPage } from './app.po';
import { browser, logging, protractor } from 'protractor';

describe('workspace-project App', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
    page.navigateTo();
  });

  it('should display welcome message', () => {
    expect(page.getTitleText()).toEqual('protractor app is running!');
  });

  it('should match the title', () => {
    page.navigateToAngularIo().then(() => {
      expect(page.getTitleAngular()).toEqual('Tour of Heroes App and Tutorial');
    });
  });

  it('should go to Github page, login and show the title', () => {
    page.navigateToAngularIo();
    browser.waitForAngularEnabled(false);
    page.getGithubRedirectButton().click();
    page.getUsernameInput().sendKeys('yagizcanyenilmez@gmail.com');
    page.getPasswordInput().sendKeys('Goztepe1925;');
    // page.getSubmitButton().click();  or next line
    page.getPasswordInput().sendKeys(protractor.Key.ENTER); // or previous commented line
    expect(page.getGithubShelfTitle()).toBe('Learn Git and GitHub without any code!');
  });

  it('should have multiple paragraphs', () => {
    expect(page.getParagraph().count()).toEqual(2);
  })

  it('should show the first paragraph', () => {
    expect(page.getParagraph().get(0).getText()).toEqual('Here are some links to help you get started:');
  });

  it('should show the first paragraph with first() method', () => {
    expect(page.getParagraph().first().getText()).toEqual('Here are some links to help you get started:');
  });

  it('should show the second paragraph', () => {
    expect(page.getParagraph().get(1).getText()).toEqual('What do you want to do next with your app?');
  });

  it('should show the last paragraph with last() method', () => {
    expect(page.getParagraph().last().getText()).toEqual('What do you want to do next with your app?');
  });

  it('should have multiple h2 tags', () => {
    expect(page.getH2Title().count()).toEqual(2);
  });

  it('should show the first h2 tag', () => {
    expect(page.getH2Title().get(0).getText()).toMatch('Resources');
  });

  it('should show the first h2 tag with first() method', () => {
    expect(page.getH2Title().first().getText()).toEqual('Resources');
  });

  it('should show the second h2 tag', () => {
    expect(page.getH2Title().get(1).getText()).toBe('Next Steps');
  });

  it('should show the last h2 tag with last() method', () => {
    expect(page.getH2Title().last().getText()).toEqual('Next Steps');
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
