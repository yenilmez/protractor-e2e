export class Variables {

    public static correctCredentials = {
        username : 'g_a1',
        password : 'a' 
     };
 
     public static wrongCredentials = {
         username : 'wrongUsername',
         password : 'wrongPassword'
     }

    public static comboboxOptions = [
        ' Application View ', ' Logical View ', ' Management View '
    ];

    public static siContextMenuLinks = [
        'Send to Primary Pane',
        'Send to Secondary Pane'
    ];

    public static shadowButtonCategories = [
        'EMERGENCY', 'LIFE SAFETY', 'SECURITY', 'SUPERVISORY',
        'TROUBLE', 'HIGH', 'MEDIUM', 'LOW', 'FAULT', 'STATUS'
    ];
    
    public static summaryLinksWithSpan = [
        'System', 'Events', 'About', 'Account', 'Notifications', 'Help'
    ];

    public static statusBarTitles = [
        'Status Bar Height', 'Pane Layout', 'Text Representation'
    ];

    public static presetFilters = [
        'Recent filters', 'Saved filters'
    ];

    public static disciplines = [
        'Building Automation', 'Building Infrastructure', 'Energy Management',
        'Fire', 'Notification', 'Security', 'Management System'
    ];

    public static types = [
        'AHU', 'Addressbook', 'Area', 'Blinds', 'Boiler', 'Burner', 'Camera', 'Change Over', 'Channel', 'Chiller',
        'Client Software','Command', 'Computer', 'Condenser', 'Configuration', 'Contact', 'Control', 'Control Function',
        'Control Panel', 'Cool Exchanger', 'Cooling Coil', 'Cooling Generation', 'Cooling Group', 'Cooling Tower',
        'Damper', 'Detector', 'Device', 'Display', 'District Hot Water', 'Document', 'Door', 'Driver', 'Effect Request',
        'Electric Heater', 'Energy Recovery', 'Evaporator',' Fan', 'Filter', 'Graphics', 'Group', 'Heat Exchanger',
        'Heat Generation', 'Heating / Cooling Group', 'Heating Coil', 'Heating Group', 'Horn', 'Humidifier','Incident Template',
        'Input', 'Journaling', 'Library Element', 'Light', 'Macro', 'Manuel', 'Message Template', 'Module', 'Monitor',
        'Motor', 'Network', 'Notification Element', 'Notification Template', 'Operating Procedure', 'Output', 'Power Supply',
        'Pre-heater', 'Printer', 'Program', 'Pump', 'Report', 'Scheduler', 'Scope', 'Section', 'Security Group', 'Security Profile',
         'Sensor', 'Sequence', 'Server Software', 'Setpoint', 'Speaker', 'Strobe', 'Switch', 'Trend', 'UPS', 'Ultra Violet',
         'User', 'Value', 'Valve', 'Ventilation', 'View Element', 'Zone', 'Other', 'Unassigned'
    ]
}