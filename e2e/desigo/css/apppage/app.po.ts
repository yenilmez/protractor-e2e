import { browser, by, element } from 'protractor';

export class AppPage {

  getAppPage() {
    return element(by.css('hfw-page'));
  }

  /* #region - First, get the snapins to check if they are loaded successfully */

  getSummarySnapin() {
    return element(by.css('gms-summary-bar-snapin'));
  }

  getSysbrowSnapin() {
    return element(by.css('gms-system-browser-snapin'));
  }

  getTextualSnapin() {
    return element(by.css('gms-text-snapin'));
  }

  getPropertySnapin() {
    return element(by.css('gms-property-snapin'));
  }

  getRelatedSnapin() {
    return element(by.css('gms-related-items-snapin'));
  }

  getDocumentSnapin() {
    return element(by.css('gms-document-snapin'));
  }

  getGraphicsSnapin() {
    return element(by.css('gms-graphics-viewer-snapin'));
  }

  getScheduleSnapin() {
    return element(by.css('gms-schedule-snapin'));
  }

  getTrendSnapin() {
    return element(by.css('gms-trend-snapin'));
  }

  /* #endregion - Get snapins */


  /* #region - Summary bar elements */

  getLogoImage() {
    return element(by.css('gms-summary-bar-snapin .brand-logo-sm img'));
  }

  getSummaryBarLinks() {
    return element.all(by.css('gms-summary-bar-snapin .nav.navbar-nav li'));
  }

  getSummaryBarLinksWithText() {
    return element.all(by.css('gms-summary-bar-snapin .nav.navbar-nav li .item-title'));
  }

  getSummaryBarLayoutBtn() {
    return element(by.css('gms-summary-bar-snapin .panel-heading .accordion-toggle .navbar-more-accordion-button'));
  }

  getSummaryBarLayout() {
    return element(by.css('gms-summary-bar-snapin gms-layout-settings'));
  }

  getLayoutTexts() {
    return element.all(by.css('gms-summary-bar-snapin gms-layout-settings .gms-layout-text'));
  }

  getStatusBarHeightOptions() {
    return element.all(by.css('gms-summary-bar-snapin gms-layout-settings .status-bar-state-selector label i.gms-layout-icons'));
  }

  getPaneLayoutOptions() {
    return element.all(by.css('gms-summary-bar-snapin gms-layout-settings .gms-layout-container .gms-layout-container i.gms-layout-icons'))
  }

  getLayoutCnsListItems() {
    return element.all(by.css('gms-summary-bar-snapin gms-layout-settings .gms-layout-cns-list-item'));
  }

  getLayoutLockBtn() {
    return element(by.css('gms-summary-bar-snapin gms-layout-settings .gms-layout-lock'));
  }

  getDefaultLayoutButton() {
    return element(by.css('gms-summary-bar-snapin gms-layout-settings .gms-layout-default-button'));
  }

  getShadowButtons() {
    return element.all(by.css('gms-summary-bar-snapin .sb-container .hfw-button-shadow'));
  }

  getShadowButtonCategories() {
    return element.all(by.css('gms-summary-bar-snapin .sb-container .hfw-button-shadow .lamp-content .span-category'));
  }

  getMuteButton() {
    return {
      'button': element(by.css('gms-summary-bar-snapin .mute-button')),
      'icon': element(by.css('gms-summary-bar-snapin .mute-button .gms-summary-bar-icons'))
    }
  }

  /* #endregion - Summary bar elements */


  /* #region - System browser elements */

  getObjectManager() {
    return element(by.css('gms-object-manager'));
  }

  getSysbrowFilterButton() {
    return element(by.css('.filter-header-controls .btn-pad-sq'));
  }

  getMutedText() {
    return element(by.css('.filter-header-controls .text-muted')).getText();
  }

  getSysbrowCancelFilterButton() {
    return element(by.css('.filter-header-container .btn-circle'));
  }

  getCombobox() {
    return element(by.css('hfw-combobox'));
  }

  getComboboxOptions() {
    return element.all(by.css('hfw-combobox option'));
  }

  getSysbrowSiTree() {
    return element(by.css('si-tree-view.hfw-flex-item-grow'));
  }

  getSysbrowSiTreeNodes() {
    return element.all(by.css('.sysbrow-si-dataField1'));
  }

  getContextMenuButtons() {
    return element.all(by.css('.si-tree-view-item .marengo-options-vertical'));
  }

  getContextMenuLinks() {
    return element.all(by.css('si-context-menu ul li a'));
  }

  getTypes() {
    return element.all(by.css('hfw-tree-selector[inputelementname="objectTypeSelector"] [style="padding-left: 0px;"] li.si-tree-view-li-hover h5'))
  }

  /* #endregion - System browser elements */

}
