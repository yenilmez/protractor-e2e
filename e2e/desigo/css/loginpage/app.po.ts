import { browser, by, element } from 'protractor';

export class LoginPage {

  navigateTo() {
    return browser.get('https://127.0.0.1:443') as Promise<any>;
  }

  getInputForUsername() {
    return element(by.css('input#username'))
  }

  getLandingPage() {
    return element(by.css('.landing-page'));
  }

  getLoginPageBackground() {
    return element(by.css('.landing-page-background'));
  }

  getLoginPageImage() {
    return element(by.css('.landing-page-logo'));
  }

  getHfwLoginPage() {
    return element(by.css('hfw-login'));
  }

  getLoginPageFormTitle() {
    return element(by.css('.landing-page-content h1')).getText();
  }

  getUsernameLabel() {
    return element(by.css("label[for='username']")).getText();
  }

  getUsernameInput() {
    return element(by.css('#username'));
  }

  getPasswordLabel() {
    return element(by.css("label[for='password']")).getText();
  }

  getPasswordInput() {
    return element(by.css('#password'));
  }

  getLoginButton() {
    return element(by.css('.btn.btn-primary'));
  }

  getAlertMessage() {
    return element(by.css('.landing-page-custom-content .alert.alert-danger'));
  }

  getOSSReadmeLink() {
    return element(by.css('.landing-page-links a[href="eula/ReadMe_OSS_GMS_Flex_Client.html"]'))
  }

  getAppPage() {
    return element(by.css('hfw-page'));
  }
}

