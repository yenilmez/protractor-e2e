import { LoginPage } from './app.po';
import { browser, logging, element, by } from 'protractor';
import { Variables } from '../../variables';

describe('Flex Client web app login page ', () => {
  let loginPage: LoginPage;

  beforeEach(() => {
    loginPage = new LoginPage();
  });

  beforeAll(() => {
    // maximize here does not work in headless mode
    // do it in protractor.conf.js as args: ['--window-size={width}x{height}']
    browser.manage().window().maximize();
  });
  
  it('should display landing page (test from url).', () => {
    loginPage.navigateTo();
    browser.getCurrentUrl().then((url) => {
      expect(url).toContain('loginpage');
    })
  });

  it('should display landing page (test from css class).', () => {
    expect(loginPage.getLandingPage().isPresent()).toBe(true);
  });

  it('should have username input', () => {
    expect(loginPage.getInputForUsername().isPresent()).toBe(true)
  });

  it('should display hfw login page.', () => {
    expect(loginPage.getHfwLoginPage().isDisplayed()).toBeTruthy();
  });

  it('should have the background image for login page.', () => {
    expect(loginPage.getLoginPageBackground().isDisplayed()).toBeTruthy();
  });

  it('should have Siemens logo on landing page.', () => {
    expect(loginPage.getLoginPageImage().isDisplayed()).toBeTruthy();
  });

  it('should have the correct form title.', () => {
    expect(loginPage.getLoginPageFormTitle()).toEqual('Desigo CC');
  });

  it('should have the correct label for user name.', () => {
    expect(loginPage.getUsernameLabel()).toEqual('User name:');
  });

  it('should have the input to insert username.', () => {
    expect(loginPage.getUsernameInput().isDisplayed()).toBeTruthy();
  });

  it('should have the correct label for password.', () => {
    expect(loginPage.getPasswordLabel()).toEqual('Password:');
  });

  it('should have the input to insert password.', () => {
    expect(loginPage.getPasswordInput().isDisplayed()).toBeTruthy();
  });

  it('should have the correct text on login button.', () => {
    expect(loginPage.getLoginButton().getText()).toEqual('Login');
  });

  it('should have the link for OSS readme.', () => {
    expect(loginPage.getOSSReadmeLink().getText()).toEqual('OSS');
  });

  it('should redirect to readme page when clicking on OSS link.', () => {
    browser.waitForAngularEnabled(false);
    loginPage.getOSSReadmeLink().click().then(() => {
      browser.getAllWindowHandles().then((handles) => {
        let newWindowHandle = handles[1];
        browser.switchTo().window(newWindowHandle).then(() => {
          browser.getCurrentUrl().then((url) => {
            expect(url).toContain('/eula/ReadMe_OSS_GMS_Flex_Client.html');
          });
        });
        browser.close();
        browser.switchTo().window(handles[0]);
      });
    });
  });

  it('should not login with wrong credentials.', () => {
    loginPage.getUsernameInput().sendKeys(Variables.wrongCredentials.username);
    loginPage.getPasswordInput().sendKeys(Variables.wrongCredentials.password);
    loginPage.getLoginButton().click();
    browser.sleep(500);
    expect(loginPage.getHfwLoginPage().isDisplayed()).toBe(true);
    browser.sleep(2000);
  });

  // it('should login with the correct credentials.', () => {
  //   loginPage.getUsernameInput().clear();
  //   loginPage.getPasswordInput().clear();
  //   loginPage.getUsernameInput().sendKeys(Variables.correctCredentials.username);
  //   loginPage.getPasswordInput().sendKeys(Variables.correctCredentials.password);
  //   loginPage.getLoginButton().click();
  //   browser.waitForAngularEnabled(false); // or waitForAngular() ?????????????????????
  //   browser.sleep(1500);
  //   expect(loginPage.getAppPage().isDisplayed()).toBe(true);
  // });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.OFF,
    } as logging.Entry));
  });
});
