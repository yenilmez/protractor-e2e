import { AppPage } from './app.po';
import { LoginPage } from '../loginpage/app.po';
import { browser, logging, element, by } from 'protractor';
import { Variables } from '../../variables';

describe('Flex Client main page after login', () => {
  let loginPage: LoginPage;
  let page: AppPage;
  let i;

  beforeEach(() => {
    page = new AppPage();
    loginPage = new LoginPage();
  });

  beforeAll(() => {
    browser.manage().window().maximize();
  });

  it('should login first', () => {
    loginPage.navigateTo();
    loginPage.getUsernameInput().sendKeys(Variables.correctCredentials.username);
    loginPage.getPasswordInput().sendKeys(Variables.correctCredentials.password);
    loginPage.getLoginButton().click();
    browser.waitForAngularEnabled(false); // or waitForAngular() ?????????????????????
    browser.sleep(1500);
  });

  it('should display app page.', () => {
    browser.waitForAngular();
    expect(page.getAppPage()).toBeDefined();
    page.getMuteButton().button.click();
  });

  it('should have the required snapins loaded', () => {
    expect(page.getSummarySnapin().isDisplayed()).toBeTruthy();
    expect(page.getSysbrowSnapin().isDisplayed()).toBeTruthy();
    expect(page.getTextualSnapin().isDisplayed()).toBeTruthy();
    expect(page.getPropertySnapin().isDisplayed()).toBeTruthy();
    expect(page.getRelatedSnapin().isDisplayed()).toBeTruthy();

    page.getSysbrowSiTreeNodes().get(2).click();
    browser.sleep(500);
    expect(page.getDocumentSnapin().isDisplayed()).toBeTruthy();

    page.getSysbrowSiTreeNodes().get(3).click();
    browser.sleep(500);
    expect(page.getGraphicsSnapin().isDisplayed()).toBeTruthy();

    page.getSysbrowSiTreeNodes().get(8).click();
    browser.sleep(500);
    expect(page.getScheduleSnapin().isDisplayed()).toBeTruthy();

    page.getSysbrowSiTreeNodes().get(9).click();
    browser.sleep(500);
    expect(page.getTrendSnapin().isDisplayed()).toBeTruthy();
    
    page.getSysbrowSiTreeNodes().get(0).click();
    browser.sleep(500);
  });
  
// ***********************************************
// *                SUMMARY BAR                  *
// ***********************************************

  it('should have Siemens logo in summary bar on left top.', () => {
    console.log('    ***** - SUMMARY BAR - *****');
    expect(page.getLogoImage().isPresent()).toBe(true);
  });

  // it('should have clickable mute button.', () => {
  //   expect(page.getMuteButton().button.isDisplayed()).toBeTruthy();
  //   expect(page.getMuteButton().icon.getAttribute('class')).toMatch('marengo-sound-on');
  //   page.getMuteButton().button.click();
  //   expect(page.getMuteButton().icon.getAttribute('class')).toMatch('marengo-sound-off')
  // });

  // Check summary bar links with span text
  it('should have correct link spans in summary bar', () => {
    for(i = 1; i <= Variables.summaryLinksWithSpan.length; i++) {
      expect(page.getSummaryBarLinksWithText().get(i-1).getAttribute('textContent')).toMatch(Variables.summaryLinksWithSpan[i-1])
    }
    expect(page.getSummaryBarLayoutBtn().element(by.css('div.float-left')).getAttribute('textContent')).toEqual('Layout');
  })

  // Check summary bar links without text
  it('should have the correct content in summary bar links.', () => {
    expect(page.getSummaryBarLinks().get(3).element(by.css('gms-notification-center')).isDisplayed()).toBeTruthy();

    page.getSummaryBarLinks().get(3).click();
    expect(page.getSummaryBarLinks().get(3).element(by.css('.dropdown-menu.drawer')).isDisplayed()).toBeTruthy();
    expect(page.getSummaryBarLinks().get(4).element(by.css('.sb-navbar-user-a')).isDisplayed()).toBeTruthy();

    page.getSummaryBarLinks().get(4).click();
    expect(page.getSummaryBarLinks().get(5).element(by.css('a span')).getAttribute('textContent')).toMatch('Logout');
    expect(page.getSummaryBarLinks().get(6).element(by.css('.marengo-options-vertical.icon')).isDisplayed()).toBeTruthy();
  });

  // Shadow button container
  it('should have correct content for shadow buttons.', () => {
    for(i = 0; i < Variables.shadowButtonCategories.length; i++) {
      expect(page.getShadowButtonCategories().get(i).getText()).toEqual(Variables.shadowButtonCategories[i]);
    }
  });

  it('should have layout.', () => {
    page.getSummaryBarLinks().get(6).click();
    page.getSummaryBarLayoutBtn().click();
    expect(page.getSummaryBarLayout().isDisplayed()).toBeTruthy();
  });

  it('should have correct texts for layout titles.', () => {
    for(i = 0; i < Variables.statusBarTitles.length; i++) {
      expect(page.getLayoutTexts().get(i).getText()).toEqual(Variables.statusBarTitles[i]);
    };
  });

  it('should display two different height options for status bar.', () => {
    let options = page.getStatusBarHeightOptions();
    expect(options.count()).toBe(2);
    for(i = 0; i < options.length; i++) {
      expect(options[i]).toMatch('marengo-status-bar-')
    }
  });

  it('should have correct icons for height options.', () => {
    expect(page.getStatusBarHeightOptions().first().getAttribute('class')).toMatch('marengo-status-bar-large');
    expect(page.getStatusBarHeightOptions().last().getAttribute('class')).toMatch('marengo-status-bar-small');
  })

  it('should display five different layout options for panes', () => {
    expect(page.getPaneLayoutOptions().count()).toBe(5);
  });

  it('should have six different cns items.', () => {
    expect(page.getLayoutCnsListItems().count()).toBe(6);
  });

  it('should have clickable lock button', () => {
    expect(page.getLayoutLockBtn().isDisplayed()).toBeTruthy();
    page.getLayoutLockBtn().click();
    page.getLayoutLockBtn().click();
  });

  it('should change the lock button view on click.', () => {
    expect(page.getLayoutLockBtn().getAttribute('class')).toMatch('marengo-unlock');   
    page.getLayoutLockBtn().click();
    expect(page.getLayoutLockBtn().getAttribute('class')).toMatch('marengo-lock-state');
  });

  it('should have clickable "Default Layout" button.', () => {
    expect(page.getDefaultLayoutButton().isDisplayed()).toBeTruthy();
    page.getDefaultLayoutButton().click();
    browser.navigate().back();
  });

  it('should redirect to events frame when clicking on "Events" link', () => {
    page.getSummaryBarLinks().get(2).click();
    browser.sleep(1000);
    browser.getCurrentUrl().then((url) => {
      expect(url).toContain('event-list');
    });
    const scrollDown = element.all(by.css('hfw-grid-cell.hfw-grid-body-cell-with-text.col-md-2[style="width: 324px;"]')).get(40);
    browser.actions().mouseMove(scrollDown).perform();
    browser.sleep(5000);
    // element(by.css('.hfw-grid-body'))
    browser.executeScript('arguments[0].scrollIntoView(true)', scrollDown.getWebElement());
    // browser.navigate().back();
    browser.sleep(2000);
  });

  it('should redirect to about frame when clicking on "About" link.', () => {
    page.getSummaryBarLinks().get(6).click();
    page.getSummaryBarLinks().get(7).click();
    browser.getCurrentUrl().then((url) => {
      expect(url).toContain('about-frame-id');
    });
    browser.navigate().back();
    browser.sleep(500);
  });

  it('should redirect to account frame when clicking on "Account" link.', () => {
    page.getSummaryBarLinks().get(6).click();
    page.getSummaryBarLinks().get(8).click();
    browser.getCurrentUrl().then((url) => {
      expect(url).toContain('account-frame-id');
    });
    browser.navigate().back();
    browser.sleep(500);
  });

  it('should redirect to notifications frame when clicking on "Notifications" link.', () => {
    page.getSummaryBarLinks().get(6).click();
    page.getSummaryBarLinks().get(9).click();
    browser.getCurrentUrl().then((url) => {
      expect(url).toContain('notifconfig-frame-id');
    });
    browser.navigate().back();
    browser.sleep(500);
  });

  it('should redirect to help page when clicking on "Help" link.', () => {
    browser.waitForAngularEnabled(false);
    page.getSummaryBarLinks().get(6).click();
    page.getSummaryBarLinks().get(10).click().then(() => {
      browser.getAllWindowHandles().then((handles) => {
        let newWindowHandle = handles[1];
        browser.switchTo().window(newWindowHandle).then(() => {
          browser.getCurrentUrl().then((url) => {
            expect(url).toContain('webhelp');
          });
        });
        browser.close();
        browser.switchTo().window(handles[0]);
      });
    });
  });

// // **********************************************
// // *                SYSTEM BROWSER              *
// // **********************************************

//   it('should include "gms-object-manager".', () => {
//     console.log('%c     ***** - SYSTEM BROWSER - *****', "color: blue");
//     expect(page.getObjectManager().isPresent()).toBeTruthy();
//   });

//   it('should have filter button.', () => {
//     expect(page.getSysbrowFilterButton().isDisplayed()).toBeTruthy();
//   });

//   it('should have the cancel button as disabled by default.', () => {
//     expect(page.getSysbrowCancelFilterButton().isEnabled()).toBeFalsy();
//   });

//   it('should have text "No filters applied." if there is no selected filter after clicking the button.', () => {
//     page.getSysbrowFilterButton().click();
//     expect(page.getMutedText()).toEqual('No filters applied.');
//   });

//   it('should have the cancel button as enabled after clicking the filter button.', () => {
//     page.getSysbrowFilterButton().click();
//     expect(page.getSysbrowCancelFilterButton().isDisplayed()).toBeTruthy();
//   });

//   it('should have combobox.', () => {
//     expect(page.getCombobox().isDisplayed()).toBeTruthy();
//   });

//   it('should have correct texts under combobox options.', () => {
//     for(i = 0; i < Variables.comboboxOptions.length; i++) {
//       expect(page.getComboboxOptions().get(i).getText()).toEqual(Variables.comboboxOptions[i]);
//     }
//   });

//   it('should have "si-tree".', () => {
//     expect(page.getSysbrowSiTree().isPresent()).toBeTruthy();
//   });

//   it('should have correct texts for si-context in system browser nodes.', () => {
//     page.getContextMenuButtons().get(0).click();
//     for (i = 0; i < Variables.siContextMenuLinks.length; i++) {
//       expect(page.getContextMenuLinks().get(i).getText()).toEqual(Variables.siContextMenuLinks[i]);
//     }
//   });

//   // it('should have correct filter types.', () => {
//   //     for(i = 0; i < Variables.types.length; i++) {
//   //       expect(page.getTypes().get(i).getText()).toEqual(Variables.types[i]);
//   //     };
//   // });

// // **********************************************
// // *                  PROPERTIES                *
// // **********************************************

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.OFF,
    } as logging.Entry));
  });
});