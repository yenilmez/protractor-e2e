import { browser, by, element } from 'protractor';

export class AppPage {

    getAppPage() {
      return element(by.xpath('//hfw-page'));
    }
  
    /* #region - First, get the snapins to check if they are loaded successfully */
  
    getSummarySnapin() {
      return element(by.xpath('//gms-summary-bar-snapin'));
    }
  
    getSysbrowSnapin() {
      return element(by.xpath('//gms-system-browser-snapin'));
    }
  
    getTextualSnapin() {
      return element(by.xpath('//gms-text-snapin'));
    }
  
    getPropertySnapin() {
      return element(by.xpath('//gms-property-snapin'));
    }
  
    getRelatedSnapin() {
      return element(by.xpath('//gms-related-items-snapin'));
    }
  
    getDocumentSnapin() {
      return element(by.xpath('//gms-document-snapin'));
    }
  
    getGraphicsSnapin() {
      return element(by.xpath('//gms-graphics-viewer-snapin'));
    }
  
    getScheduleSnapin() {
      return element(by.xpath('//gms-schedule-snapin'));
    }
  
    getTrendSnapin() {
      return element(by.xpath('//gms-trend-snapin'));
    }
  
    /* #endregion - Get snapins */
  
  
    /* #region - Summary bar elements */
  
    getLogoImage() {
      return element(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "brand-logo-sm")]//img'));
    }
  
    getSummaryBarLinks() {
      return element.all(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "nav") and contains(@class, "navbar-nav")]//li'));
    }
  
    getSummaryBarLinksWithText() {
      return element.all(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "nav") and contains(@class, "navbar-nav")]//li//span[@class="item-title"]'));
    }
  
    getSummaryBarLayoutBtn() {
      return element(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "panel-heading")]//*[contains(@class, "accordion-toggle")]//*[contains(@class, "navbar-more-accordion-button")]'));
    }
  
    getSummaryBarLayout() {
      return element(by.xpath('//gms-summary-bar-snapin//gms-layout-settings'));
    }
  
    getLayoutTexts() {
      return element.all(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//*[contains(@class, "gms-layout-text")]'));
    }
  
    getStatusBarHeightOptions() {
      return element.all(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//div[@class="status-bar-state-selector"]//label//i[contains(@class, "gms-layout-icons")]'));
    }
  
    getPaneLayoutOptions() {
      return element.all(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//div[@class="gms-layout-container"]//i[contains(@class, "gms-layout-icons")]'))
    }
  
    getLayoutCnsListItems() {
      return element.all(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//div[@class="gms-layout-cns-list-item"]'));
    }
  
    getLayoutLockBtn() {
      return element(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//button[contains(@class, "gms-layout-lock")]'));
    }
  
    getDefaultLayoutButton() {
      return element(by.xpath('//gms-summary-bar-snapin//gms-layout-settings//*[contains(@class, "gms-layout-default-button")]'));
    }
  
    getShadowButtons() {
      return element.all(by.xpath('//gms-summary-bar-snapin//div[contains(@class, "sb-container")]//button[contains(@class, "hfw-button-shadow")]'));
    }
  
    getShadowButtonCategories() {
      return element.all(by.xpath('//gms-summary-bar-snapin//div[contains(@class, "sb-container")]//button[contains(@class, "hfw-button-shadow")]//div[@class="lamp-content"]//span[@class="span-category"]'));
    }
  
    getMuteButton() {
      return {
        'button': element(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "mute-button")]')),
        'icon': element(by.xpath('//gms-summary-bar-snapin//*[contains(@class, "mute-button")]//*[contains(@class, "gms-summary-bar-icons")]'))
      }
    }
  
    /* #endregion - Summary bar elements */
  
  
    /* #region - System browser elements */
  
    getObjectManager() {
      return element(by.xpath('//gms-object-manager'));
    }
  
    getSysbrowFilterButton() {
      return element(by.xpath('//div[@class="filter-header-controls"]//button[contains(@class, "btn-pad-sq")]'));
    }
  
    getMutedText() {
      return element(by.xpath('//div[@class="filter-header-controls"]//i[@class="text-muted"]')).getText();
    }
  
    getSysbrowCancelFilterButton() {
      return element(by.xpath('//div[@class="filter-header-container"]//button[contains(@class, "btn-circle")]'));
    }
  
    getCombobox() {
      return element(by.xpath('//hfw-combobox'));
    }
  
    getComboboxOptions() {
      return element.all(by.xpath('//hfw-combobox//option'));
    }
  
    getSysbrowSiTree() {
      return element(by.xpath('//si-tree-view[@class="hfw-flex-item-grow"]'));
    }
  
    getSysbrowSiTreeNodes() {
      return element.all(by.xpath('//*[contains(@class, "sysbrow-si-dataField1")]'));
    }
  
    getContextMenuButtons() {
      return element.all(by.xpath('//div[@class="si-tree-view-item"]//*[contains(@class, "marengo-options-vertical")]'));
    }
  
    getContextMenuLinks() {
      return element.all(by.xpath('//si-context-menu//ul//li//a'));
    }
  
    getTypes() {
      return element.all(by.xpath('//hfw-tree-selector[inputelementname="objectTypeSelector"]//*[@style="padding-left: 0px;"]//li[contains(@class, "si-tree-view-li-hover")]//h5'))
    }
  
    /* #endregion - System browser elements */
  
  }