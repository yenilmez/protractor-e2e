import { browser, by, element } from 'protractor';

export class LoginPage {

  navigateTo() {
    return browser.get('https://127.0.0.1:443') as Promise<any>;
  }

  getLandingPage() {
    return element(by.xpath('//div[contains(@class, "landing-page")]'));
  }

  getLoginPageBackground() {
    return element(by.xpath('//div[@class="landing-page-background"]'));
  }

  getLoginPageImage() {
    return element(by.xpath('//img[@class="landing-page-logo"]'));
  }

  getHfwLoginPage() {
    return element(by.xpath('//hfw-login'));
  }

  getLoginPageFormTitle() {
    return element(by.xpath('//div[contains(@class, "landing-page-content")]//h1')).getText();
  }

  getUsernameLabel() {
    return element(by.xpath("//label[@for='username']")).getText();
  }

  getUsernameInput() {
    return element(by.xpath('//*[@id="username"]'));
  }

  getPasswordLabel() {
    return element(by.xpath("//label[@for='password']")).getText();
  }

  getPasswordInput() {
    return element(by.xpath('//*[@id="password"]'));
  }

  getLoginButton() {
    return element(by.xpath("//button[@class='btn btn-primary']"));
  }

  getOSSReadmeLink() {
    return element(by.xpath('//*[contains(@href, "ReadMe")]'));
    // return element(by.xpath('//*[@href="eula/ReadMe_OSS_GMS_Flex_Client.html"]'));
  }

  getAppPage() {
    return element(by.xpath('//hfw-page'));
  }
}