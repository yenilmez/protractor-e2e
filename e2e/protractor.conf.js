// @ts-check
// Protractor configuration file, see link for more information
// https://github.com/angular/protractor/blob/master/lib/config.ts

const { SpecReporter } = require('jasmine-spec-reporter');

/**
 * @type { import("protractor").Config }
 */
exports.config = {
  allScriptsTimeout: 11000,
  specs: [
  ],
  multiCapabilities: [
    {
      'browserName': 'chrome',
      chromeOptions: {
      // binary: process.env.CHROME_BIN,
      // to run in headless mode, window size should be set here.
      // not possible to do it in spec file.
      args: ['--window-size=1920x1080', '--ignore-certificate-errors', '--no-sandbox', '--headless']
      }
    }
    // you can add other capability objects for different browsers.
  ],
  directConnect: true,
  baseUrl: 'http://localhost:4200/',
  framework: 'jasmine',
  jasmineNodeOpts: {
    showColors: true,
    defaultTimeoutInterval: 30000,
    print: function() {}
  },
  suites: {
    loginPageCss: './desigo/css/loginpage/app.e2e-spec.ts'
  //   loginPageXPath: './desigo/xpath/loginpage/app.e2e-spec.ts',
  //   appPageCss: './desigo/css/apppage/app.e2e-spec.ts',
  //   appPageXPath: './desigo/xpath/apppage/app.e2e-spec.ts'
  },
  onPrepare() {
    require('ts-node').register({
      project: require('path').join(__dirname, './tsconfig.json')
    });
    jasmine.getEnv().addReporter(new SpecReporter({ spec: { displayStacktrace: true } }));
  }
};